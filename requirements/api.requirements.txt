-r base.requirements.txt
fastapi==0.75.1
uvicorn==0.17.6
gunicorn==20.1.0
marshmallow-sqlalchemy==0.28.0
