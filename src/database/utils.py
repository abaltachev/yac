import typing as t

from src.database.models import SessionLocal


def get_or_create(session: SessionLocal, model: t.Any, **kwargs):
    """
    It creates a new model instance or returns the existing one
    """
    instance = session.query(model).filter_by(**kwargs).first()
    if not instance:
        instance = model(**kwargs)
        session.add(instance)
        session.flush()
    return instance
