import typing as t

from pydantic import BaseModel, Field
from sqlalchemy import Column, ForeignKey, Integer, String, create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

from src.database.settings import DATABASE

Base = declarative_base()


def db_connect() -> Engine:
    return create_engine(URL(**DATABASE))


def create_table(engine: Engine):
    Base.metadata.create_all(engine)


class Manufacturer(Base):
    __tablename__ = "manufacturer"

    id = Column(Integer, primary_key=True, autoincrement=True)
    manufacturer_name = Column("manufacturer_name", String)
    categories = relationship("Category", backref="manufacturer")


class Category(Base):
    __tablename__ = "category"

    id = Column(Integer, primary_key=True, autoincrement=True)
    category_name = Column("category_name", String)
    manufacturer_id = Column(Integer, ForeignKey("manufacturer.id"))
    models = relationship("Model", backref="category")


class Model(Base):
    __tablename__ = "model"

    id = Column(Integer, primary_key=True, autoincrement=True)
    model_name = Column("model_name", String)
    category_id = Column(Integer, ForeignKey("category.id"))
    part_categories = relationship("PartCategory", backref="model")


class PartCategory(Base):
    __tablename__ = "part_category"

    id = Column(Integer, primary_key=True, autoincrement=True)
    part_category_name = Column("part_category_name", String)
    model_id = Column(Integer, ForeignKey("model.id"))
    part_numbers = relationship("Part", backref="part_category")


class Part(Base):
    __tablename__ = "part"

    id = Column(Integer, primary_key=True, autoincrement=True)
    part_number = Column("part_number", String)
    part_category_id = Column(Integer, ForeignKey("part_category.id"))


class CatalogueItem(BaseModel):
    """
    The schema describes a parsed catalogue item (catalogue sections and an item number)
    """

    manufacturer_name: t.Optional[str] = Field(
        None, description="Company name of a part. For example: JCB, Ammann"
    )
    category_name: t.Optional[str] = Field(
        None,
        description="Category name of a part. For example: Engines, Excavator Parts",
    )
    model_name: t.Optional[str] = Field(
        None, description="Model name of a part. For example: CX1200W, KH180"
    )
    part_category_name: t.Optional[str] = Field(
        None, description="Part category of a part. For example: NAME-PLATE, BRAKE"
    )
    part_number: t.Optional[str] = Field(
        None, description="Part number of a part. For example: ND021228 , ND021195"
    )

    @classmethod
    def parse_list(cls, values):
        return cls.parse_obj(dict(zip(cls.__fields__, values)))


SessionLocal = sessionmaker(bind=db_connect())


def get_session() -> t.Generator[SessionLocal, None, None]:
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()
