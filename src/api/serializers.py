from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow_sqlalchemy.fields import Nested

from src.database.models import Category, Manufacturer, Model, Part, PartCategory


class PartSchema(SQLAlchemySchema):
    class Meta:
        model = Part
        ordered = True

    part_number = auto_field()


class PartCategorySchema(SQLAlchemySchema):
    class Meta:
        model = PartCategory
        ordered = True

    part_category_name = auto_field()
    part_numbers = Nested(PartSchema, many=True)


class ModelSchema(SQLAlchemySchema):
    class Meta:
        model = Model
        ordered = True

    model_name = auto_field()
    part_categories = Nested(PartCategorySchema, many=True)


class CategorySchema(SQLAlchemySchema):
    class Meta:
        model = Category
        ordered = True

    category_name = auto_field()
    models = Nested(ModelSchema, many=True)


class ManufacturerCategorySchema(SQLAlchemySchema):
    class Meta:
        model = Manufacturer
        ordered = True

    manufacturer_name = auto_field()
    categories = Nested(CategorySchema, many=True)
