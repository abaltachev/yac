import typing as t

from sqlalchemy import and_
from sqlalchemy.orm import Session, contains_eager
from sqlalchemy.sql.elements import BinaryExpression

from src.database.models import (
    CatalogueItem,
    Category,
    Manufacturer,
    Model,
    Part,
    PartCategory,
)


def get_items(
    session: Session, filters: t.List[BinaryExpression]
) -> t.List[Manufacturer]:
    """
    It retrieves joined table rows filtered by specified filter functions
    """
    return (
        session.query(Manufacturer)
        .outerjoin(Category)
        .outerjoin(Model)
        .outerjoin(PartCategory)
        .outerjoin(Part)
        .options(
            contains_eager(Manufacturer.categories),
            contains_eager(Manufacturer.categories, Category.models),
            contains_eager(
                Manufacturer.categories, Category.models, Model.part_categories
            ),
            contains_eager(
                Manufacturer.categories,
                Category.models,
                Model.part_categories,
                PartCategory.part_numbers,
            ),
        )
        .filter(and_(*filters))
        .all()
    )


def get_filters(item: CatalogueItem) -> t.List[BinaryExpression]:
    """
    It returns filter functions for a specific class
    """
    if isinstance(item, CatalogueItem):
        name2model = {
            "manufacturer_name": Manufacturer.manufacturer_name,
            "category_name": Category.category_name,
            "model_name": Model.model_name,
            "part_category_name": PartCategory.part_category_name,
            "part_number": Part.part_number,
        }
        filters = [
            name2model[field_name] == field_value
            for field_name, field_value in item.dict().items()
            if field_value is not None and field_name in name2model
        ]
    else:
        raise Exception("[%s] class does not have any filters", item.__class__)

    return filters
