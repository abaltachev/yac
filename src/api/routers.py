from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from src.api.queries import get_filters, get_items
from src.api.serializers import ManufacturerCategorySchema
from src.database.models import CatalogueItem, get_session

router = APIRouter(prefix="/api")
manufactorer_serializer = ManufacturerCategorySchema()


@router.get("/catalogue/")
def get_catalogue_items(
    item: CatalogueItem = Depends(), session: Session = Depends(get_session)
):
    filters = get_filters(item)
    items = get_items(session, filters)
    return [manufactorer_serializer.dump(item) for item in items]
