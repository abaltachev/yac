import os

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from src.scraper.scraper.spiders.catalogue_spider import CatalogueSpider

if __name__ == "__main__":
    os.environ.setdefault("SCRAPY_SETTINGS_MODULE", "src.scraper.scraper.settings")
    settings = get_project_settings()
    process = CrawlerProcess(settings=settings)

    process.crawl(CatalogueSpider)
    process.start()
