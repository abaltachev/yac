import logging

from src.database.models import (
    CatalogueItem,
    Category,
    Manufacturer,
    Model,
    Part,
    PartCategory,
    SessionLocal,
    create_table,
    db_connect,
)
from src.database.utils import get_or_create

logger = logging.getLogger(__name__)
logger.warning("This is a warning")


class CrawlPipeline:
    field_name2model = {
        "manufacturer_name": Manufacturer,
        "category_name": Category,
        "model_name": Model,
        "part_category_name": PartCategory,
        "part_number": Part,
    }

    field_name2parent_id = {
        "manufacturer_name": None,
        "category_name": "manufacturer_id",
        "model_name": "category_id",
        "part_category_name": "model_id",
        "part_number": "part_category_id",
    }

    def __init__(self):
        create_table(db_connect())

    def process_item(self, item: CatalogueItem, spider) -> CatalogueItem:
        """
        It processes parsed CatalogueItem object and saves it to db.
        """
        session = SessionLocal()

        try:
            parent_instance = None
            for field_name, field_value in item.dict().items():
                if not field_value:
                    break

                field_name2field_value = {field_name: field_value}
                if parent_instance:
                    field_name2field_value[
                        self.field_name2parent_id[field_name]
                    ] = parent_instance.id

                parent_instance = get_or_create(
                    session, self.field_name2model[field_name], **field_name2field_value
                )
            session.commit()
        except Exception:
            logger.exception("Impossible to save [%s] item to db", item)
            session.rollback()
        finally:
            session.close()

        return item
