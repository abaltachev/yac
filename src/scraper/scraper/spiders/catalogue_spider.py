import typing as t
from string import Template
from urllib.parse import urlparse

import scrapy
from scrapy.http import Response

from src.database.models import CatalogueItem


class CatalogueSpider(scrapy.Spider):
    name = "catalogue"
    allowed_domains = ["urparts.com"]
    start_urls = ["https://www.urparts.com/index.cfm/page/catalogue"]
    base_css_selector = Template("div.c_container.$element_path ul li a")
    css_selectors = [
        base_css_selector.substitute(element_path="allmakes"),
        base_css_selector.substitute(element_path="allmakes.allcategories"),
        base_css_selector.substitute(element_path="allmodels"),
        base_css_selector.substitute(element_path="allparts"),
    ]
    selectors_amount = len(css_selectors)

    def get_item(self, response: Response) -> t.Optional[CatalogueItem]:
        """
        It parses the navigation section of the website to extract required items.
        """
        path_parts = response.css("#path a::text").extract()

        if not path_parts:
            self.logger.warning(
                "Impossible to extract path parts in [%s]. Skipping...", response.url
            )
            return

        path_parts = [
            part.strip() for part in path_parts[2:]
        ]  # skip unnecessary parts of a path (Home and Catalogue)

        if "part=" in urlparse(response.url).query:
            part_number = response.css(
                "div.c_container.c_partDetail p #partNo::attr(value)"
            ).get()
            if part_number:
                path_parts.append(part_number.strip())
            else:
                self.logger.warning(
                    "Impossible to extract a part number in [%s]. Skipping...",
                    response.url,
                )
        return CatalogueItem.parse_list(path_parts)

    def parse(
        self, response: Response, depth: t.Optional[int] = None
    ) -> t.Generator[CatalogueItem, None, None]:
        """
        It goes through the catalog (sections, subsections, subsubsections) and parse items of every section.
        It stops iteration whether depth exceeds sections amount or a css selector can not find required items.
        """
        if depth is None:
            depth = 0

        if depth < self.selectors_amount and (
            items_selectors := response.css(self.css_selectors[depth])
        ):
            for selector in items_selectors:
                url = selector.css("::attr(href)").get()
                yield response.follow(url, self.parse, cb_kwargs=dict(depth=depth + 1))
        else:
            item = self.get_item(response)
            self.logger.info("[%s] was parsed", item)
            if item:
                yield item
