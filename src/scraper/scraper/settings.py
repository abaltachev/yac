import os

BOT_NAME = "scraper"

SPIDER_MODULES = ["src.scraper.scraper.spiders"]
NEWSPIDER_MODULE = "src.scraper.scraper.spiders"

ITEM_PIPELINES = {
    "src.scraper.scraper.pipelines.CrawlPipeline": 300,
}

LOG_LEVEL = os.environ.get("LOG_LEVEL", "INFO")
CONCURRENT_REQUESTS = os.environ.get("CONCURRENT_REQUESTS", 32)

COOKIES_ENABLED = False
ROBOTSTXT_OBEY = True
