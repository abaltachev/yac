# YAC (Yet another crawler)

Web crawler built with [scrapy](https://scrapy.org/)\
So far, it only parses [Urparts catalogue](https://www.urparts.com/index.cfm/page/catalogue)

## Project structure

- [Scraper and FastAPI project](src)
- [Docker compose files for deployment](deployment)
- [Project requirements](requirements)

### FAQ

#### What env variables I have to specify before deployment?

All of them are listed in [deployment/configs/.env](deployment/configs/.env)

- `LOG_LEVEL` - Loglevel of root service logger, accepts loglevels from https://docs.python.org/3/library/logging.html#logging-levels, for example `INFO`, `WARNING`, etc, (default=`INFO`). 
   
- `POSTGRES_DB` - It can be used to define a different name for the default database that is created when the image is first started (default=`urparts`).
   
- `POSTGRES_USER` - It is used in conjunction with POSTGRES_PASSWORD to set a user and its password. This variable will create the specified user with superuser power and a database with the same name (default=`postgres`).

- `POSTGRES_PASSWORD` - This environment variable is required for you to use the PostgreSQL image. It must not be empty or undefined. This environment variable sets the superuser password for PostgreSQL. (default=`KEAVXGWz26Wj4zr4`).

- `POSTGRES_HOST` - Name of the container with PostgreSQL db (default=`db`).

- `POSTGRES_PORT` - Port of PostgreSQL db (default=`5432`)
  
- `CONCURRENT_REQUESTS` - The maximum number of concurrent (i.e. simultaneous) requests that will be performed by the Scrapy downloader (default=`64`).

- `NUM_WORKERS` - The amount of Gunicorn workers (default=`4`)

#### How to run the crawler?
```bash
docker-compose -f deployment/db/docker-compose.yml \
               -f deployment/crawler/docker-compose.yml \
               --project-name dnl \
               --env-file deployment/configs/.env up --build
```

#### How to run the api?
```bash
docker-compose -f deployment/db/docker-compose.yml \
               -f deployment/api/docker-compose.yml \
               --project-name dnl \
               --env-file deployment/configs/.env up --build
```

Go to the `127.0.0.1:8001/docs` to see Swagger API documentation
